from typing import Annotated

from fastapi import FastAPI, File, UploadFile
from os import getcwd
import os
import win32com.client
import base64

app = FastAPI()


@app.post("/uploadfile/")
async def create_upload_file(file: UploadFile):
    with open(getcwd() + "/" + file.filename, "wb") as myfile:
        content = await file.read()
        myfile.write(content)
        myfile.close()
        wdFormatPDF = 17
        inputFile = os.path.abspath(file.filename)
        name = file.filename.replace('.doc', '.pdf')
        outputFile = os.path.abspath(name)
        word = win32com.client.Dispatch("Word.Application")
        doc = word.Documents.Open(inputFile)
        doc.SaveAs(outputFile, FileFormat=wdFormatPDF)
        doc.Close()
        word.Quit()
        os.remove(inputFile)
        with open(name, "rb") as pdf_file:
            encodedString = base64.b64encode(pdf_file.read())
    return {"pdf": encodedString}
    
    